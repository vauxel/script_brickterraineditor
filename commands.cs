// ============================================================
// Project          -      Brick Terrain Editor
// Description      -      Server Commands Code
// ============================================================

function servercmdBTE(%client, %firstParam, %secondParam, %thirdParam) {
	if(!strLen(%firstParam)) {
		messageclient(%client, '', "\c1[BTE]\c6 Brick Terrain Editor v1.0 by Vaux. Type \"/bte commands\" for a list of commands");
		return;
	}

	if(strLwr(%firstParam) $= "commands") {
		messageclient(%client, '', "\c1[BTE]\c6 List of Brick Terrain Editor commands:");
		messageclient(%client, '', "\c1[BTE]\c6 /bte \c7brushes (bs)\c6 - Lists all of the available brushes");
		messageclient(%client, '', "\c1[BTE]\c6 /bte \c7brush (b)\c6 - Selects the specified brush");
		messageclient(%client, '', "\c1[BTE]\c6 /bte \c7brushsize (bs)\c6 - Changes the brush's effect range/size");
		messageclient(%client, '', "\c1[BTE]\c6 /bte \c7brushcolor (bc)\c6 - Changes the brush's color");
		messageclient(%client, '', "\c1[BTE]\c6 /bte \c7info (i)\c6 - Displays the currently selected brush type and size");
		return;
	}

	if((strLwr(%firstParam) $= "brushlist") || (strLwr(%firstParam) $= "bl")) {
		messageclient(%client, '', "\c1[BTE]\c6 List of Brick Terrain Editor brushes:");

		for(%i = 0; %i < BTEBrushManager.getCount(); %i++) {
			%brushName = BTEBrushManager.getObject(%i).name;
			%brushAliases = BTEBrushManager.getObject(%i).aliases;
			%brushDescription = BTEBrushManager.getObject(%i).description;
			messageclient(%client, '', "\c1[BTE]\c6 [" @ %brushName @ "] \c7(" @ %brushAliases @ ")\c6 - " @ %brushDescription);
		}

		return;
	}

	if((strLwr(%firstParam) $= "info") || (strLwr(%firstParam) $= "i")) {
		%selectedBrush = BTEBrushManager.getBrush(%client.BTE_selectedBrush);

		if(isObject(%selectedBrush)) {
			if(%client.BTE_selectedBrushOption !$= "") {
				messageclient(%client, '', "\c1[BTE]\c6 Selected Brush:\c7" SPC %selectedBrush.name SPC "[" @ getWord(%selectedBrush.options, %client.BTE_selectedBrushOption - 1) @ "]");
			} else {
				messageclient(%client, '', "\c1[BTE]\c6 Selected Brush:\c7" SPC %selectedBrush.name);
			}
		} else {
			messageclient(%client, '', "\c1[BTE]\c6 Selected Brush:\c7" SPC "None");
		}

		messageclient(%client, '', "\c1[BTE]\c6 Selected Brush Size:\c7" SPC %client.BTE_selectedBrushSize);
		messageclient(%client, '', "\c1[BTE]\c6 Selected Brush Color:\c7" SPC %client.BTE_selectedBrushColor);

		return;
	}

	if((strLwr(%firstParam) $= "brush") || (strLwr(%firstParam) $= "b")) {
		if(!strLen(%secondParam)) {
			messageclient(%client, '', "\c1[BTE]\c6 You have to enter a brush's name to select it");
			return;
		}

		if(!strLen(%brushIndex = BTEBrushManager.findBrushByName(%secondParam))) {
			messageclient(%client, '', "\c1[BTE]\c7 \"" @ %secondParam @ "\" \c6is not a valid brush name!");
			return;
		}

		%brush = BTEBrushManager.getBrush(%brushIndex);

		if(strLen(%thirdParam)) {
			if(!strLen(%brush.options) || (getWordCount(%brush.options) == 0)) {
				messageclient(%client, '', "\c1[BTE] The\c7" SPC %brush.name SPC "\c6 Brush does not have any options");
				return;
			}

			for(%i = 0; %i < getWordCount(%brush.options); %i++) {
				if(strLwr(getWord(%brush.options, %i)) $= strLwr(%thirdParam)) {
					%index = %i;
					break;
				}
			}

			if(!strLen(%index)) {
				messageclient(%client, '', "\c1[BTE]\c7 \"" @ %thirdParam @ "\" \c6is not a valid brush option for the\c7" SPC %brush.name SPC "\c6Brush!");
				return;
			}

			BTEBrushManager.selectBrush(%client, %brushIndex);
			BTEBrushManager.selectBrushOption(%client, %index + 1);
			messageclient(%client, '', "\c1[BTE]\c6 You have selected the\c7" SPC %brush.name SPC "\c6Brush with the \c7\"" @ getWord(%brush.options, %index) @ "\"\c6 Option");
			return;
		}

		BTEBrushManager.selectBrush(%client, %brushIndex);
		messageclient(%client, '', "\c1[BTE]\c6 You have selected the\c7" SPC %brush.name SPC "\c6Brush");

		if(getWordCount(%brush.options) > 0) {
			messageclient(%client, '', "\c1[BTE]\c6 Valid options for the\c7" SPC %brush.name SPC "\c6Brush: [\c7" @ strReplace(%brush.options, " ", ", ") @ "\c6]");
		}
		
		return;
	}

	if((strLwr(%firstParam) $= "brushsize") || (strLwr(%firstParam) $= "bs")) {
		if(!strLen(%secondParam) || !isInteger(%secondParam)) {
			messageclient(%client, '', "\c1[BTE]\c6 You have to enter a valid integer brush size \c7(1-" @ $BTE::Config::Brush::MaxSize @ ")");
			return;
		}

		if((%secondParam < 1) || (%secondParam > $BTE::Config::Brush::MaxSize)) {
			messageclient(%client, '', "\c1[BTE]\c7 \"" @ %secondParam @ "\" \c6is not a valid brush size \c7(1-" @ $BTE::Config::Brush::MaxSize @ ")!");
			return;
		}

		BTEBrushManager.selectBrushSize(%client, %secondParam);
		messageclient(%client, '', "\c1[BTE]\c6 You have selected a brush size of\c7" SPC %secondParam);
		return;
	}

	if((strLwr(%firstParam) $= "brushcolor") || (strLwr(%firstParam) $= "bc")) {
		if(!strLen(%secondParam) || !isInteger(%secondParam)) {
			messageclient(%client, '', "\c1[BTE]\c6 You have to enter a valid integer color column number from the spray can palette");
			return;
		}

		if(!strLen(%thirdParam) || !isInteger(%thirdParam)) {
			messageclient(%client, '', "\c1[BTE]\c6 You have to enter a valid integer color row number from the spray can palette");
			return;
		}

		if((%secondParam <= 0) || (%secondParam > PaintRowGroup.getCount())) {
			messageclient(%client, '', "\c1[BTE]\c7 \"" @ %secondParam @ "\" \c6is not a valid color column number \c7(1-" @ PaintRowGroup.getCount() @ ")!");
			return;
		}

		if((%thirdParam <= 0) || (%thirdParam > PaintRowGroup.getObject(%secondParam).numSwatches)) {
			messageclient(%client, '', "\c1[BTE]\c7 \"" @ %thirdParam @ "\" \c6is not a valid color row number \c7(1-" @ PaintRowGroup.getObject(%secondParam).numSwatches @ ")!");
			return;
		}

		for(%i = 0; %i < (%secondParam - 1); %i++) {
			%paintNumber += PaintRowGroup.getObject(%i).numSwatches;
		}

		%paintNumber += %thirdParam;
		%paintNumber -= 1;

		BTEBrushManager.selectBrushColor(%client, %paintNumber);
		messageclient(%client, '', "\c1[BTE]\c6 You have selected a brush color of\c7" SPC %paintNumber);
		return;
	}
}