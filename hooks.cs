// ============================================================
// Project          -      Brick Terrain Editor
// Description      -      Default Function Hooks Code
// ============================================================

package BrickTerrainEditor_Hooks {
	function gameConnection::autoAdminCheck(%client) {
		parent::autoAdminCheck(%client);

		%client.BTE_selectedBrush = BTEBrushManager.findBrushByName($BTE::Config::Brush::DefaultType);
		%client.BTE_selectedBrushSize = $BTE::Config::Brush::DefaultSize;
		%client.BTE_selectedBrushColor = $BTE::Config::Brush::DefaultColor;
	}

	function fxDTSBrickData::onPlant(%this, %brick) {
		Parent::onPlant(%this, %brick);

		if(BTETerrainManager.isTerrainBrick(%brick)) {
			%brick.isBaseplate = true;
		}
	}
};

if(isPackage(BrickTerrainEditor_Hooks))
	deactivatepackage(BrickTerrainEditor_Hooks);
activatepackage(BrickTerrainEditor_Hooks);