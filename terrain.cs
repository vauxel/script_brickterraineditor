// ============================================================
// Project          -      Brick Terrain Editor
// Description      -      Terrain Manager Code
// ============================================================

if(!isObject(BTETerrainManager)) {
	new ScriptObject(BTETerrainManager);
}

function BTETerrainManager::snapVectorToGrid(%this, %vector) {
	%x = mFloatLength(getWord(%vector, 0), 0) - (mFloatLength(getWord(%vector, 0), 0) % ($BTE::Config::Terrain::BrickDatablock.brickSizeZ / 5));
	%y = mFloatLength(getWord(%vector, 1), 0) - (mFloatLength(getWord(%vector, 1), 0) % ($BTE::Config::Terrain::BrickDatablock.brickSizeZ / 5));
	%z = mFloatLength(getWord(%vector, 2), 0) - (mFloatLength(getWord(%vector, 2), 0) % ($BTE::Config::Terrain::BrickDatablock.brickSizeZ / 5));

	return (%x SPC %y SPC %z);
}

function BTETerrainManager::convertToBTECoordinates(%this, %vector) {
	%vector = BTETerrainManager.snapVectorToGrid(%vector);
	%div = ($BTE::Config::Terrain::BrickDatablock.brickSizeZ / 5);
	%vector = (getWord(%vector, 0) / %div) SPC (getWord(%vector, 1) / %div) SPC (getWord(%vector, 2) / %div);
	return %vector;
}

function BTETerrainManager::convertToWorldCoordinates(%this, %vector) {
	%vector = vectorScale(%vector, ($BTE::Config::Terrain::BrickDatablock.brickSizeZ / 5));
	%vector = %this.snapVectorToGrid(%vector);
	return %vector;
}

function BTETerrainManager::isTerrainBrick(%this, %brick) {
	if(!isObject(%brick)) {
		if($BTE::Debug) warn("BTETerrainManager::isTerrainBrick() ==> Invalid \"%brick\" given -- does not exist");
		return;
	}

	if(%brick.getPosition() !$= %this.snapVectorToGrid(%brick.getPosition())) {
		return false;
	}

	if(%brick.getDatablock().getName() !$= $BTE::Config::Terrain::BrickDatablock) {
		return false;
	}

	return true;
}

function BTETerrainManager::setTerrainBrick(%this, %vector, %color) {
	if(getWord(%vector, 2) <= 0) {
		if($BTE::Debug) warn("BTETerrainManager::setTerrainBrick() ==> Vector position given is below ground-level [" @ %vector @ "]");
		return;
	}

	%vector = %this.convertToWorldCoordinates(%vector);

	%brick = new fxDTSBrick() {
		datablock = $BTE::Config::Terrain::BrickDatablock;
		position = %vector;
		rotation = "0 0 0 0";
		scale = "1 1 1";
		angleID = 0;
		colorID = (strLen(%color) ? %color : $BTE::Config::Brush::DefaultColor);
		stackBL_ID = 888888;
		isPlanted = 1;
	};

	%error = %brick.plant();
	%brick.setTrusted(1);

	if(%error == 1) {
		%brick.delete();
		if($BTE::Debug) warn("BTETerrainManager::setTerrainBrick() ==> A Terrain brick already exists at [" @ %vector @ "]");
		return;
	}

	BrickGroup_888888.add(%brick);
}

function BTETerrainManager::removeTerrainBrick(%this, %vector) {
	if(isObject(%brick = %this.getTerrainBrick(%vector))) {
		if($BTE::Debug) warn("BTETerrainManager::removeTerrainBrick() ==> A Terrain brick does not exist at [" @ %vector @ "]");
		return;
	}

	BrickGroup_888888.remove(%brick);
	%brick.delete();
}

function BTETerrainManager::getTerrainBrick(%this, %x, %y, %z) {
	%vector = %this.convertToWorldCoordinates(%vector);
	%x = getWord(%vector, 0); %y = getWord(%vector, 1); %z = getWord(%vector, 2);

	if(mFloor(getWord(%brick.rotation, 3)) == 90) {
		%boxSize = (%y SPC %x SPC %z);
	}
	else {
		%boxSize = (%x SPC %y SPC %z);
	}
				
	initContainerBoxSearch(((%x * 2) SPC (%y * 2) SPC (%z * 2)), %boxSize, $TypeMasks::FxBrickObjectType);
				
	while(isObject(%brick = containerSearchNext())) {
		if(BTETerrainManager.isTerrainBrick(%brick) == true) {
			return %brick;
		}
	}
}