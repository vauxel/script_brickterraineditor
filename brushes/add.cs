// ============================================================
// Project          -      Brick Terrain Editor
// Description      -      Add Brush Code
// ============================================================

if(!isObject(BTEAddBrush)) {
	BTEBrushManager.add(new ScriptObject(BTEAddBrush) {
		name = "Add";
		aliases = "add a";
		description = "Adds a single cube of Terrain";
		type = "origin";
	});
}

function BTEAddBrush::use(%this, %option, %pos, %brushSize, %brushColor) {
	BTETerrainManager.setTerrainBrick(%pos, %brushColor);
}