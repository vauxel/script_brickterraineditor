// ============================================================
// Project          -      Brick Terrain Editor
// Description      -      Ball Brush Code
// ============================================================

if(!isObject(BTEBallBrush)) {
	BTEBrushManager.add(new ScriptObject(BTEBallBrush) {
		name = "Ball";
		aliases = "ball b";
		description = "Creates a ball of Terrain surrounding the target";
		type = "origin";
		options = "filled hollow";
	});
}

function BTEBallBrush::use(%this, %option, %pos, %brushSize, %brushColor) {
	for(%x = -%brushSize; %x <= %brushSize; %x++) {
		for(%y = -%brushSize; %y <= %brushSize; %y++) {
			for(%z = -%brushSize; %z <= %brushSize; %z++) {
				if(%x * %x + %y * %y + %z * %z <= %brushSize * %brushSize) {
					if(%option == 2) {
						if(mCeil(vectorDist(%pos, vectorAdd(%pos, %x SPC %y SPC %z))) < %brushSize) {
							continue;
						}
					}

					BTETerrainManager.setTerrainBrick(vectorAdd(%pos, %x SPC %y SPC %z), %brushColor);
				}
			}
		}
	}
}