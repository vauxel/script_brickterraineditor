// ============================================================
// Project          -      Brick Terrain Editor
// Description      -      Ball Brush Code
// ============================================================

if(!isObject(BTEDiscBrush)) {
	BTEBrushManager.add(new ScriptObject(BTEDiscBrush) {
		name = "Disc";
		aliases = "disc d";
		description = "Creates a disc of Terrain surrounding the target";
		type = "origin";
		options = "normal smoothed";
	});
}

function BTEDiscBrush::use(%this, %option, %pos, %brushSize, %brushColor) {
	for(%x = -%brushSize; %x <= %brushSize; %x++) {
		for(%y = -%brushSize; %y <= %brushSize; %y++) {
			if(%option == 1) {
				if(%x * %x + %y * %y <= %brushSize * %brushSize) {
					BTETerrainManager.setTerrainBrick(vectorAdd(%pos, %x SPC %y SPC 0), %brushColor);
				}
			} else if(%option == 2) {
				if(%x * %x + %y * %y <= %brushSize * %brushSize + %brushSize * 0.8) {
					BTETerrainManager.setTerrainBrick(vectorAdd(%pos, %x SPC %y SPC 0), %brushColor);
				}
			}
		}
	}
}