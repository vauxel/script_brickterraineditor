// ============================================================
// Project          -      Brick Terrain Editor
// Description      -      Square Brush Code
// ============================================================

if(!isObject(BTECubeBrush)) {
	BTEBrushManager.add(new ScriptObject(BTECubeBrush) {
		name = "Cube";
		aliases = "cube c";
		description = "Creates a cube of Terrain surrounding the target";
		type = "origin";
		options = "filled hollow";
	});
}

function BTECubeBrush::use(%this, %option, %pos, %brushSize, %brushColor) {
	for(%x = -%brushSize; %x <= %brushSize; %x++) {
		for(%y = -%brushSize; %y <= %brushSize; %y++) {
			for(%z = -%brushSize; %z <= %brushSize; %z++) {
				if(%option == 2) {
					if(((%x != -%brushSize) && (%x != %brushSize)) && ((%y != -%brushSize) && (%y != %brushSize)) && ((%z != -%brushSize) && (%z != %brushSize))) {
						continue;
					}
				}

				BTETerrainManager.setTerrainBrick(vectorAdd(%pos, %x SPC %y SPC %z), %brushColor);
			}
		}
	}
}