// ============================================================
// Project          -      Brick Terrain Editor
// Description      -      Add-On Config
// ============================================================

$BTE::Debug = false;

$BTE::Config::Terrain::BrickDatablock = brick4xCubeData;

$BTE::Config::Brush::MaxDistance = 100;
$BTE::Config::Brush::DefaultType = "Add";
$BTE::Config::Brush::DefaultSize = 5;
$BTE::Config::Brush::DefaultColor = 2; // 2 = Green
$BTE::Config::Brush::MaxSize = 25;