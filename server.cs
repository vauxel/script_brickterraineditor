// ============================================================
// Project          -      Brick Terrain Editor
// Description      -      Add-On Initialization
// ============================================================

exec("./config.cs");
exec("./terrain.cs");
exec("./brushes.cs");
exec("./commands.cs");
exec("./hooks.cs");