// ============================================================
// Project          -      Brick Terrain Editor
// Description      -      Brushes Code
// ============================================================
// Sections
//   1: Brush Wand
//   2: Brush Manager
// ============================================================

// ============================================================
// Section 1 - Brush Wand
// ============================================================

datablock particleData(BTEBrushWandParticle) {
	colors[0] = "0 0.75 0 0.9";
	colors[1] = "0 0.75 0 0.9";
	colors[2] = "0 0 0 0.9";
	colors[3] = "0 0 0 0.9";

	gravityCoefficient = 0;

	lifetimeMS = 500;
	lifetimeVarianceMS = 0;

	sizes[0] = 0.10;
	sizes[1] = 0.15;
	sizes[2] = 0.20;

	spinRandomMax = 90;
	spinRandomMin = -90;

	textureName = "base/data/particles/chunk";

	times[1] = 0.2;
	times[2] = 1;
};

datablock particleEmitterData(BTEBrushWandEmitter) {
	particles = BTEBrushWandParticle;

	ejectionOffset = 0.09;
	ejectionPeriodMS = 5;
	ejectionVelocity = 0.3;

	periodVarianceMS = 0;
	thetaMax = 180;
	velocityVariance = 0;
};

datablock ProjectileData(BTEBrushWandProjectile) {
	projectileShapeName = "base/data/shapes/empty.dts";
};

datablock ItemData(BTEBrushWandItem) {
	category = "Weapon";
	className = "Weapon";

	shapeFile = "base/data/shapes/wand.dts";
	rotate = false;
	mass = 1;
	density = 0.2;
	elasticity = 0.2;
	friction = 0.6;
	emap = true;

	uiName = "Terraformer";
	iconName = "base/client/ui/itemIcons/Wand";
	doColorShift = true;
	colorShiftColor = "0 0.5 0 1.000";

	image = BTEBrushWandImage;
	canDrop = true;
};

datablock ShapeBaseImageData(BTEBrushWandImage : WandImage) {
	className = "WeaponImage";

	item = BTEBrushWandItem;
	ammo = " ";
	projectile = BTEBrushWandProjectile;
	projectileType = Projectile;

	melee = false;
	armReady = true;

	doColorShift = true;
	colorShiftColor = BTEBrushWandItem.colorShiftColor;

	stateEmitter[1] = BTEBrushWandEmitter;
};

function BTEBrushWandImage::onPreFire(%this, %obj, %slot) {
	%obj.playThread(2, "armAttack");
}

function BTEBrushWandImage::onStopFire(%this, %obj, %slot) {
	%obj.playThread(2, "root");
}

function BTEBrushWandImage::onFire(%this, %obj, %slot) {
	Parent::onFire(%this, %obj, %slot);

	%camera = %obj.client.getControlObject();
	%vector = vectorAdd(vectorScale(vectorNormalize(%camera.getEyeVector()), $BTE::Config::Brush::MaxDistance), %camera.getEyePoint());
	%raycast = containerRayCast(%camera.getEyePoint(), %vector, $TypeMasks::FxBrickObjectType | $TypeMasks::TerrainObjectType, %camera);
	%collision = firstWord(%raycast);
	%rayNormal = normalFromRaycast(%raycast);

	if(!isObject(%collision)) {
		return;
	}

	if(%collision.getClassName() $= "fxDTSBrick") {
		%pos = vectorAdd(%collision.getPosition(), vectorScale(%rayNormal, ($BTE::Config::Terrain::BrickDatablock.brickSizeZ / 5)));
	} else if(%collision.getClassName() $= "fxPlane") {
		%pos = vectorAdd(posFromRaycast(%raycast), "0 0" SPC ($BTE::Config::Terrain::BrickDatablock.brickSizeZ / 5));
	}
	
	BTEBrushManager.useSelectedBrush(%obj.client, %pos);
}

// ============================================================
// Section 1 - Brush Manager
// ============================================================

if(!isObject(BTEBrushManager)) {
	new ScriptGroup(BTEBrushManager) {
		brushesPath = "./brushes/";
	};

	BTEBrushManager.schedule(100, "loadBrushes");
}

function BTEBrushManager::loadBrushes(%this) {
	if(!strLen(%this.brushesPath)) {
		if($BTE::Debug) warn("BTEBrushManager::loadBrushes() ==> BTEBrushManager's \"brushesPath\" has not been set");
		return;
	}

	for(%brushFile = findFirstFile(%this.brushesPath @ "*.cs"); strLen(%brushFile); %brushFile = findNextFile(%this.brushesPath @ "*.cs")) {
		exec(%brushFile);
		if($BTE::Debug) echo("BTEBrushManager::loadBrushes() :: Loaded brush, \"" @ strReplace(%brushFile, %this.brushesPath, "") @ "\"");
	}
}

function BTEBrushManager::getBrush(%this, %index) {
	if(!isObject(%this.getObject(%index))) {
		if($BTE::Debug) warn("BTEBrushManager::findBrushByAlias() ==> Invalid \"%index\" given -- out of range or nonexistent");
		return;
	}

	return %this.getObject(%index);
}

function BTEBrushManager::findBrushByAlias(%this, %alias) {
	if(!strLen(%alias = strLwr(%alias))) {
		if($BTE::Debug) warn("BTEBrushManager::findBrushByAlias() ==> Invalid \"%alias\" given -- cannot be blank");
		return;
	}

	for(%i = 0; %i < %this.getCount(); %i++) {
		if(!isObject(%this.getObject(%i))) {
			continue;
		}

		for(%j = 0; %j < getWordCount(%this.getObject(%i).aliases); %j++) {
			if(strLwr(getWord(%this.getObject(%i).aliases, %j)) $= %alias) {
				return %i;
			}
		}
	}
}

function BTEBrushManager::findBrushByName(%this, %name) {
	if(!strLen(%name = strLwr(%name))) {
		if($BTE::Debug) warn("BTEBrushManager::findBrushByName() ==> Invalid \"%name\" given -- cannot be blank");
		return;
	}

	for(%i = 0; %i < %this.getCount(); %i++) {
		if(!isObject(%this.getObject(%i))) {
			continue;
		}

		if(strLwr(%this.getObject(%i).name) $= %name) {
			return %i;
		}
	}
}

function BTEBrushManager::useSelectedBrush(%this, %client, %rawPos) {
	if(!strLen(%client) || !isObject(%client)) {
		if($BTE::Debug) warn("BTEBrushManager::useSelectedBrush() ==> Invalid \"%client\" given -- cannot be blank or nonexistent");
		return;
	}

	if(!strLen(%rawPos)) {
		if($BTE::Debug) warn("BTEBrushManager::useSelectedBrush() ==> Invalid \"%rawPos\" given -- cannot be blank");
		return;
	}

	%selectedBrush = %client.BTE_selectedBrush;

	if(getWordCount(%rawPos) != 3) {
		if($BTE::Debug) warn("BTEBrushManager::useSelectedBrush() ==> Invalid \"%rawPos\" given -- Vector3 not given");
		return;
	}

	%pos = BTETerrainManager.convertToBTECoordinates(%rawPos);
	%this.getBrush(%selectedBrush).use(%client.BTE_selectedBrushOption, %pos, %client.BTE_selectedBrushSize, %client.BTE_selectedBrushColor);
}

function BTEBrushManager::selectBrush(%this, %client, %index) {
	if(!strLen(%client) || !isObject(%client)) {
		if($BTE::Debug) warn("BTEBrushManager::selectBrush() ==> Invalid \"%client\" given -- cannot be blank or nonexistent");
		return;
	}

	if(!isObject(%brush = %this.getBrush(%index))) {
		if($BTE::Debug) warn("BTEBrushManager::selectBrush() ==> Invalid \"%index\" given -- matching brush does not exist");
		return;
	}

	if(!%this.isMember(%brush)) {
		if($BTE::Debug) warn("BTEBrushManager::selectBrush() ==> Invalid \"%brush\" retrieved from \"%index\"");
		return;
	}

	%client.BTE_selectedBrush = %index;

	if(getWordCount(%brush.options) > 0) {
		%this.selectBrushOption(%client, 1);
	} else {
		%this.selectBrushOption(%client, -1);
	}
}

function BTEBrushManager::selectBrushSize(%this, %client, %size) {
	if(!strLen(%client) || !isObject(%client)) {
		if($BTE::Debug) warn("BTEBrushManager::selectBrushSize() ==> Invalid \"%client\" given -- cannot be blank or nonexistent");
		return;
	}

	if(!strLen(%size) || !isInteger(%size)) {
		if($BTE::Debug) warn("BTEBrushManager::selectBrushSize() ==> Invalid \"%size\" given -- cannot be blank or non-integer");
		return;
	}

	%client.BTE_selectedBrushSize = %size;
}

function BTEBrushManager::selectBrushColor(%this, %client, %id) {
	if(!strLen(%client) || !isObject(%client)) {
		if($BTE::Debug) warn("BTEBrushManager::selectBrushSize() ==> Invalid \"%client\" given -- cannot be blank or nonexistent");
		return;
	}

	if(!strLen(%id) || !isInteger(%id)) {
		if($BTE::Debug) warn("BTEBrushManager::selectBrushSize() ==> Invalid \"%id\" given -- cannot be blank or non-integer");
		return;
	}

	%client.BTE_selectedBrushColor = %id;
}

function BTEBrushManager::selectBrushOption(%this, %client, %index) {
	if(!strLen(%client)) {
		if($BTE::Debug) warn("BTEBrushManager::selectBrushSize() ==> Invalid \"%client\" given -- cannot be blank or nonexistent");
		return;
	}

	if((%index $= "") || (%index == -1)) {
		%client.BTE_selectedBrushOption = "";
		return;
	}

	if(!strLen(getWord(%this.getBrush(%client.BTE_selectedBrush).options, %index - 1))) {
		if($BTE::Debug) warn("BTEBrushManager::selectBrush() ==> Invalid \"%index\" given -- matching brush option does not exist");
		return;
	}

	%client.BTE_selectedBrushOption = %index;
}